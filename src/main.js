// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Notifications from 'vue-notification'
import AxiosPlugin from './plugins/AxiosPlugin';
import VueLocalStorage from 'vue-localstorage';
import _ from 'lodash';

Vue.config.productionTip = false

Vue.use(Notifications)
Vue.use(require('vue-moment'));
Vue.use(AxiosPlugin)
Vue.use(VueLocalStorage)

/* eslint-disable no-new */
const NotFound = { template: '<p>Página não encontrada</p>' }
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',

  currentRoute: window.location.pathname,
  
  computed: {
    ViewComponent () {
      return routes[this.currentRoute] || NotFound
    }
  },
});
