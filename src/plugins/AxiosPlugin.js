import axios from 'axios';
axios.defaults.headers['Access-Control-Allow-Origin'] = '*';
axios.defaults.headers['Content-Type'] ='application/x-www-form-urlencoded';
axios.defaults.headers['Access-Control-Allow-Credentials'] =  'true';
export default (V) => {
  Object.defineProperties(V.prototype, {
    
    $http: {
      get() {
        return axios;
      },
      post() {
        return axios;
      },
    },
  });
};
